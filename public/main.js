fetchHtml("header");
fetchHtml("footer");

function fetchHtml(name) {
  fetch("./"+name+".html")
      .then(response => {
        return response.text()
      })
      .then(data => {
        document.querySelector(name).innerHTML = data;
      });
}